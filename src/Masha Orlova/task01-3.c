#include <stdio.h>

int main()
{
	int i = 0, sum = 0, strlen=255;
	char str[256];

	printf("Enter the string\n");
	fgets(str, strlen, stdin);
	for (i; i <= strlen; i++)
	{
		if (str[i] >= '0' && str[i] <= '9')
			sum = sum + str[i] - '0';
	}
	printf("sum of the numbers in the string: %d\n", sum);
	return 0;
}